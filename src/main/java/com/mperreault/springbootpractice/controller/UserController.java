package com.mperreault.springbootpractice.controller;

import java.util.List;

import com.mperreault.springbootpractice.dto.UserDto;
import com.mperreault.springbootpractice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/users", "/users/"})
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping
	public List<UserDto> getAllUsers() {
		return userService.getAllUsers();
	}

	@RequestMapping(method= RequestMethod.POST)
	public void saveUser(@RequestBody UserDto userDto) {
		userService.saveUser(userDto);
	}

	@RequestMapping("{userId}")
	public UserDto getUserById(@PathVariable Integer userId) {
		return userService.getUserById(userId);
	}
}
