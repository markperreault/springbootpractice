package com.mperreault.springbootpractice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value={"", "/"})
public class HomeController {

	@RequestMapping
	public String rootRedirect() {
		return "redirect:/home";
	}

	@RequestMapping(value={"home/"})
	public String homeRedirect() {
		return "redirect:/home";
	}

	@RequestMapping(value={"home"})
	public String home() {
		return "index";
	}
}
