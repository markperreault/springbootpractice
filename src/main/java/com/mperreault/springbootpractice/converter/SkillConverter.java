package com.mperreault.springbootpractice.converter;


import com.mperreault.springbootpractice.dto.SkillDto;
import com.mperreault.springbootpractice.entity.Skill;


public class SkillConverter {
	protected static Skill dtoToEntity(SkillDto SkillDto) {
		Skill Skill = new Skill(SkillDto.getSkillName(), null);
		Skill.setSkillId(SkillDto.getSkillId());
		return Skill;
	}

	protected static SkillDto entityToDto(Skill skill) {

		return new SkillDto(skill.getSkillId(), skill.getSkillName());
	}
}
