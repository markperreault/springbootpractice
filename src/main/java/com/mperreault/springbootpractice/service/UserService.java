package com.mperreault.springbootpractice.service;

import java.util.List;

import com.mperreault.springbootpractice.dto.UserDto;


public interface UserService {
    UserDto getUserById(Integer userId);
    void saveUser(UserDto userDto);
    List<UserDto> getAllUsers();
}
